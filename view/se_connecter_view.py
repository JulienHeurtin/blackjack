from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from service.recuperer_donnees_utilisateur import RecupererDonneesUtilisateur
from service.blackjack import Blackjack
import os 
from view.abstract_view import AbstractView


class Se_connecter(AbstractView):
    """
    Classe permettant de se connceter au jeu. 
    """

    def __init__(self) -> None:
        super().__init__()
        self.__questions = inquirer.select(
            message=f"Bonjour, désirez vous vous connecter et potentiellement perdre votre argent ? ",
            choices=[
                Choice("Se connecter"),
                Choice("Retour à l'acceuil"),
            ],
        )
    
    def clear_console(self) : 
        if os.name == 'posix':
            os.system('clear')
    
    def se_connecter(self):
        """
        Méthode permettant à l'utilisateur de se connecter à son compte.
        """

        # on accorde trois attentives à l'utilisateur pour se connecter
        compteur = 1
        print("Vous avez 3 essai pour vous connecter, au-delà vous devrez créer un utilisateur")
        while compteur <= 3:
            print("essai numero", compteur)

            ASK_PSEUDO = inquirer.text(message=" pseudo : ")
            
            # récupération du pseudonyme et du mot de passe de l'utilisateur
            pseudorep = ASK_PSEUDO.execute()
            
            # récupération des détails du compte de l'utilisateur
            check = RecupererDonneesUtilisateur(pseudorep)
            temp = check.verification_existence_utilisateur_db()
            utilisateur_bj = check.recuperation_donnees()
            if temp == False : 
                compteur += 1
            else : 
                self.clear_console()
                self.display_info(banniere="banner.txt", message="")
                game = Blackjack(utilisateur_bj)
                game.initialisation_jeu()
                while True:
                    choice = input("Voulez vous augmenter ou arreter: ")
                    if choice.lower() == 'augmenter':
                        game.augmenter()
                        if game.calcul_cartes(game.main_joueur.cartes) > 21:
                            game.evaluation()
                    elif choice.lower() == 'arreter':
                        game.arreter()
                    else:
                        print("Pas possible. Choisir 'augmenter' ou 'arreter', merci.")
        if compteur==4:
            from view.creation_compte import CreationCompte
            return CreationCompte()
                
            
        

    def display_info(self, banniere: str = "banner.txt", message: str = None) -> None:
        """
        fonction qui va déterminer l'affichage en console.
        Elle ne prend pas de parmaètres d'entrée et et retourne None
        Elle sera instanciée à l'échelle fille.
        """
        with open(f"graphical_assets/{banniere}", "r", encoding="utf-8") as asset:
            print(asset.read())

        if message != "" and message is not None:
            print(message + "\n")
            
    def make_choice(self) -> AbstractView:
        """
        fonction qui va gérer les choix de l'utilisateur et l'envoyer vers une autre page.
        Elle ne prend pas d'arguments et retourne None
        Elle sera instanciée à l'échelle fille.

        Affiche la partie intéractive de notre page
        """
        # création d'une instance d'utilisateur non identifié si nécessaire

        reponse = self.__questions.execute()
        if reponse == "Se connecter":
            self.clear_console()
            return self.se_connecter()
        elif reponse == "Retour à l'acceuil":
            self.clear_console()
            from view.acceuil import Acceuil
            return Acceuil()


   