"""
Module abstractview.py
"""
from abc import ABC, abstractmethod


class AbstractView(ABC):
    """
    Toute nos Views vont hériter de la classe AbstractView.
    Cette classe va contenir deux méthodes abstraites :
        display_info() : qui va juste déterminer l'affichage en console
        make_choice() : qui va gérer les choix de l'utilisateur et l'envoyer vers une autre page.
    """

    @abstractmethod
    def display_info(self) -> None:
        """
        fonction qui va déterminer l'affichage en console.
        Elle ne prend pas de parmaètres d'entrée et et retourne None
        Elle sera instanciée à l'échelle fille.
        """
        pass

    @abstractmethod
    def make_choice(self) -> None:
        """
        fonction qui va gérer les choix de l'utilisateur et l'envoyer vers une autre page.
        Elle ne prend pas d'arguments et retourne None
        Elle sera instanciée à l'échelle fille.
        """
        pass
