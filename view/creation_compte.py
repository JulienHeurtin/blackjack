"""
Module utilisateur_lambda_view.py
"""

from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from model.utilisateur import Utilisateur
from model.utilisateur import Utilisateur
import os 
from view.abstract_view import AbstractView
from service.utilisateur_creation_a_dao import UtilisateurCreationADao


class CreationCompte(AbstractView):
    """
    Classe permettant d'afficher l'acceuil du jeu, se connecter et de s'inscrire. 
    """

    def __init__(self) -> None:
        super().__init__()
        self.__questions = inquirer.select(
            message=f"Bonjour, souhaitez vous continuer la création de votre compte ? ",
            choices=[
                Choice("Continuer"),
                Choice("Retour au menu principal "),
            ],
        )
    def clear_console(self) : 
        if os.name == 'posix':
            os.system('clear')


    def display_info(self, banniere: str = "banner.txt", message: str = None) -> None:
        """
        fonction qui va déterminer l'affichage en console.
        Elle ne prend pas de parmaètres d'entrée et et retourne None
        Elle sera instanciée à l'échelle fille.
        """
        with open(f"graphical_assets/{banniere}", "r", encoding="utf-8") as asset:
            print(asset.read())

        if message != "" and message is not None:
            print(message + "\n")
            
    def make_choice(self) -> AbstractView:
        """
        fonction qui va gérer les choix de l'utilisateur et l'envoyer vers une autre page.
        Elle ne prend pas d'arguments et retourne None
        Elle sera instanciée à l'échelle fille.

        Affiche la partie intéractive de notre page
        """
        # création d'une instance d'utilisateur non identifié si nécessaire

        reponse = self.__questions.execute()
        if reponse == "Continuer":
            self.clear_console()
            self.creer_compte()
        elif reponse == "Retour au menu principal":
            self.clear_console()
            from view.acceuil import Acceuil 
            return Acceuil()

    def creer_compte(self):
        """
        Méthode permettant à l'utilisateur de créer un compte.
        """
        
        ASK_PRENOM = inquirer.text(message=" Prénom : ")
        ASK_NOM = inquirer.text(message=" Nom :")
        ASK_PSEUDO = inquirer.text(
                message=" Pseudo :",
                validate=lambda pseudo: pseudo.isalnum(),
                invalid_message="Le pseudonyme ne doit contenir que des lettres et des chiffres.",
            )
        ASK_SOLDE = inquirer.text(message=" Combien d'argent désirrez vous mettre sur le site ? : ")

            # récupération du pseudonyme et du mot de passe de l'utilisateur
        print(
                "Veuillez renseigner vos informations pour créer votre compte"
            )
        Rprenom = ASK_PRENOM.execute()
        Rnom = ASK_NOM.execute()
        Rpseudo = ASK_PSEUDO.execute()
        Rsolde = ASK_SOLDE.execute()
        if int(Rsolde) <= 0 : 
            print("Vous ne pouvez pas jouer si vous n'avez pas d'argent sur votre compte, veuillez vous recréer un compte en y mettant de l'argent !!")
            from view.acceuil import Acceuil 
            return Acceuil()
            
        utilisateur = Utilisateur(
                nom= Rnom,
                prenom=Rprenom,
                pseudo = Rpseudo,
                solde = Rsolde  
            )
        verif_unicite=UtilisateurCreationADao(utilisateur).ajout_a_db()
        if verif_unicite != "Votre compte a bien été crée":
            print("Le pseudo que vous avez choisi existe déjà, veuillez en choisir un autre")
            return CreationCompte().creer_compte()
        self.clear_console()
        print("Votre compte a bien été crée veuillez vous connecter")
        from view.se_connecter_view import Se_connecter
        return Se_connecter.se_connecter(self)