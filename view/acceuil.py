from InquirerPy import inquirer
from InquirerPy.base.control import Choice
import os 
from view.abstract_view import AbstractView

class Acceuil(AbstractView):
    """
    Classe permettant d'afficher l'acceuil du jeu, se connecter et de s'inscrire. 
    """

    def __init__(self) -> None:
        super().__init__()
        self.__questions = inquirer.select(
            message=f"Bonjour, comment allez vous ? envie d'une petite partie ? ",
            choices=[
                Choice("Se connecter"),
                Choice("Créer un compte "),
                Choice("Quitter"),
            ],
        )
    def clear_console(self) : 
        if os.name == 'posix':
            os.system('clear')


    def display_info(self, banniere: str = "banner.txt", message: str = None) -> None:
        """
        fonction qui va déterminer l'affichage en console.
        Elle ne prend pas de parmaètres d'entrée et et retourne None
        Elle sera instanciée à l'échelle fille.
        """
        with open(f"graphical_assets/{banniere}", "r", encoding="utf-8") as asset:
            print(asset.read())

        if message != "" and message is not None:
            print(message + "\n")
            
    def make_choice(self) -> AbstractView:
        """
        fonction qui va gérer les choix de l'utilisateur et l'envoyer vers une autre page.
        Elle ne prend pas d'arguments et retourne None
        Elle sera instanciée à l'échelle fille.

        Affiche la partie intéractive de notre page
        """
        # création d'une instance d'utilisateur non identifié si nécessaire

        reponse = self.__questions.execute()
        if reponse == "Se connecter":
            self.clear_console()
            from view.se_connecter_view import Se_connecter
            return Se_connecter()
        elif reponse == "Créer un compte ":
            self.clear_console()
            from view.creation_compte import CreationCompte
            return CreationCompte()
        elif reponse == "Quitter":
            return None