import sqlite3 

connection = sqlite3.connect('joueurs_bj.db')
cursor = connection.cursor()

# On créer la base avec la table utilisateur. Le pseudo est unique et servira à se connecter. Il n'y a pas de gestion
#de mot de passe. 
try:
    cursor.execute('''
    CREATE TABLE Utilisateur(
        pseudo text UNIQUE PRIMARY KEY,
        prenom text,
        nom text,
        solde int
    );
    ''')
    cursor.close()
except sqlite3.OperationalError:
    print("Tout est prêt pour jouer")
