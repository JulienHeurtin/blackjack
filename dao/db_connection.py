import os
import sqlite3
import dotenv



class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
        
class DBConnection(metaclass=Singleton):
    """
    Technical class to open only one connection to the DB.
    """
    def __init__(self):
        dotenv.load_dotenv()
        # Open the connection. 
        self.connection = sqlite3.connect(os.environ["DATABASE"])
        
    def get_connection(self):
        """
        return the opened connection
        """
        return self.connection
