import sqlite3
from model.utilisateur import Utilisateur
from dao.db_connection import DBConnection
class GestionUtilisateur:
    """
    Cette classe permet de gérer les attributs d'un utilisateur dans la base de données.
    """
    
    def __init__(self, pseudo):
        """
        Initialise la classe avec le pseudo de l'utilisateur à gérer.
        """
        self.utilisateur = Utilisateur(pseudo=pseudo,nom="Null",prenom="Null",solde=0)
        
    def recuperer_donnees_utilisateur(self):
        """
        Récupère les informations de l'utilisateur dans la base de données.
        """
        with DBConnection().get_connection() as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT prenom, nom, solde FROM Utilisateur WHERE pseudo = ?", (self.utilisateur.pseudo,))
            result = cursor.fetchone()
            if result is not None:
                prenom, nom, solde = result
                self.utilisateur.prenom = prenom
                self.utilisateur.nom = nom
                self.utilisateur.solde = solde
            else:
                return None
    
    def modifier_solde(self, nouveau_solde):
        """
        Modifie le solde de l'utilisateur dans la base de données.
        """
        with DBConnection().get_connection() as connection:
            cursor = connection.cursor()
            cursor.execute("UPDATE Utilisateur SET solde = ? WHERE pseudo = ?", (nouveau_solde, self.utilisateur.pseudo,))
            connection.commit()
            return True
