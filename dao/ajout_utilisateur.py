from model.utilisateur import Utilisateur
from dao.db_connection import DBConnection
import sqlite3

class AjoutUtilisateur:
    """
    Cette classe permet la stockage des comptes des utilisateurs dans la base de données
    """
    def creer_utilisateur(self, nouvel_utilisateur : Utilisateur):
        """
        Cette méthode prend en argument un utilisateur, qu'elle stocke dans la base de données.
        """
        try:
            connection = DBConnection()
            connection = connection.get_connection()
            utilisateur = ("{}".format(nouvel_utilisateur.prenom), "{}".format(nouvel_utilisateur.nom), "{}".format(nouvel_utilisateur.pseudo), nouvel_utilisateur.solde)
            requete = "INSERT INTO Utilisateur (prenom, nom, pseudo, solde) VALUES (?, ?, ?, ?)"
            cursor = connection.cursor()
            cursor.execute(requete, utilisateur)
            connection.commit()
            return "Votre compte a bien été crée"
        except sqlite3.IntegrityError:
            return "Le pseudo '{}' existe déjà dans la base de données.".format(nouvel_utilisateur.pseudo)

