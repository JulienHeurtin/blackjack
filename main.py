import dotenv
from view.acceuil import Acceuil
import dao.init_db

# Ce sript est le fichier à exécuter pour lancer l'application.
# Ceci le véritable point d'entrée de la notre application est une instance
# de la classe Acceui_view

dotenv.load_dotenv()
if __name__ == "__main__":
    # lancement de la page d'accueil de l'application
    current_view = Acceuil()

    #  tant que la page courante dans la console n'est pas vide, l'affichage se poursuit
    while current_view:
        # Affichage des informations de la page courante (bannière + message)
        current_view.display_info()
        # choix de l'utilisateur dans la console pour se page d'accueil
        current_view = current_view.make_choice()