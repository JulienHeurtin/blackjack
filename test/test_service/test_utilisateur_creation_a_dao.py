import unittest
from unittest.mock import patch, Mock
from model.utilisateur import Utilisateur
from dao.ajout_utilisateur import AjoutUtilisateur
from service.utilisateur_creation_a_dao import UtilisateurCreationADao

class TestUtilisateurCreationADao(unittest.TestCase):

    def test_ajout_a_db(self):
        # Créer un utilisateur à ajouter dans la base de données
        nouvel_utilisateur = Utilisateur(pseudo='test_ajout_db', nom='test', prenom='test', solde=100)
        
        # Mock de la méthode creer_utilisateur de AjoutUtilisateur pour éviter d'ajouter un véritable utilisateur dans la base de données
        with patch.object(AjoutUtilisateur, 'creer_utilisateur', return_value='Utilisateur ajouté avec succès') as mock_creer_utilisateur:
            
            # Appel de la méthode ajout_a_db de UtilisateurCreationADao pour ajouter l'utilisateur
            dao = UtilisateurCreationADao(nouvel_utilisateur)
            resultat = dao.ajout_a_db()
            
            # Vérification que la méthode creer_utilisateur de AjoutUtilisateur a bien été appelée avec le bon utilisateur en paramètre
            mock_creer_utilisateur.assert_called_once_with(nouvel_utilisateur)
            
            # Vérification que le résultat de la méthode ajout_a_db est bien le résultat renvoyé par la méthode creer_utilisateur de AjoutUtilisateur
            self.assertEqual(resultat, 'Utilisateur ajouté avec succès')

if __name__ == '__main__':
    unittest.main()
