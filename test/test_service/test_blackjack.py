import unittest
from unittest.mock import patch
from io import StringIO
from service.blackjack import Blackjack
from model.utilisateur import Utilisateur
import sys

class TestBlackjack(unittest.TestCase):
    
    def setUp(self):
        utilisateur = Utilisateur(pseudo="Donatien", nom="Daunat", prenom="Tiens", solde=100)
        self.blackjack = Blackjack(utilisateur)
        
    def test_calcul_cartes(self):
        self.assertEqual(self.blackjack.calcul_cartes([2, 3, 4]), 9)
        self.assertEqual(self.blackjack.calcul_cartes([11, 3]), 14)
        self.assertEqual(self.blackjack.calcul_cartes([10, 10, 10]), 30)
    
    def test_fonctionnement_jeu(self):
        with patch('builtins.input', side_effect=['10', 'augmenter', 'arreter', 'non']):
            with patch('builtins.print') as mock_print:
                self.blackjack.initialisation_jeu()
                self.blackjack.augmenter()
                with self.assertRaises(SystemExit):
                    self.blackjack.arreter()

    def test_print_main(self):
        self.assertIsNone(self.blackjack.print_main())
        
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_main_output(self, mock_stdout):
        self.blackjack.main_joueur.cartes = [2, 3]
        self.blackjack.print_main()
        self.assertEqual(mock_stdout.getvalue().strip(), "Votre main:[2, 3], soit un score de 5")
    
    def test_rejouer_exit(self):
        with self.assertRaises(SystemExit):
            with patch('builtins.input', return_value='non'):
                self.blackjack.rejouer()
   
if __name__ == '__main__':
    unittest.main()
