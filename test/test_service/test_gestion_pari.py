import unittest
from unittest.mock import patch, MagicMock
from service.gestion_pari import GestionPari
from model.solde_joueur import SoldeJoueur
from model.utilisateur import Utilisateur

class TestGestionPari(unittest.TestCase):

    def setUp(self):
        utilisateur = Utilisateur(pseudo="Donatien", nom="Daunat", prenom="Tiens", solde=100)
        self.gestion_pari = GestionPari(utilisateur)

    @patch('builtins.input', return_value='50')
    def test_parier_with_valid_input(self, mock_input):
        self.gestion_pari.parier()
        self.assertEqual(self.gestion_pari.mise, 50)
        self.assertEqual(self.gestion_pari.solde.utilisateur.solde, 50)

    @patch('builtins.input', side_effect=['0', '50'])
    def test_parier_with_zero_input(self, mock_input):
        self.gestion_pari.parier()
        self.assertEqual(self.gestion_pari.mise, 50)
        self.assertEqual(self.gestion_pari.solde.utilisateur.solde, 50)

    @patch('builtins.input', side_effect=['a', '100'])
    def test_parier_with_invalid_input(self, mock_input):
        self.gestion_pari.parier()
        self.assertEqual(self.gestion_pari.mise, 100)
        self.assertEqual(self.gestion_pari.solde.utilisateur.solde, 0)

    
    def test_pari_gagne(self):
        self.gestion_pari.mise = 50
        self.gestion_pari.pari_gagne()
        self.assertEqual(self.gestion_pari.solde.utilisateur.solde, 200)

if __name__ == '__main__':
    unittest.main()
