import unittest
from model.deck import Deck

class TestDeck(unittest.TestCase):

    def test_creation_deck(self):
        deck = Deck()
        deck.creation()
        self.assertIsNotNone(deck.deck_id)
        self.assertIsNotNone(deck.cartes_restantes)

    def test_est_melange(self):
        deck = Deck()
        deck.creation()
        self.assertTrue(deck.est_melange())

    def test_piocher_cartes(self):
        deck = Deck()
        deck.creation()
        cartes_restantes_avant_pioches= deck.cartes_restantes
        pioche = deck.piocher_cartes(5)
        self.assertIsNotNone(pioche)
        self.assertEqual(deck.cartes_restantes, cartes_restantes_avant_pioches - 5)

    def test_piocher_cartes_plus_que_cartes_restantes(self):
        deck = Deck()
        deck.creation()
        cartes_restantes = deck.cartes_restantes
        pioche = deck.piocher_cartes(cartes_restantes+1)
        self.assertEqual(pioche, "Il y a plus de cartes à piocher que des cartes restantes dans le deck")

if __name__ == '__main__':
    unittest.main()
