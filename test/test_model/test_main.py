import unittest
from model.main_joueur import MainJoueur
from model.deck import Deck

class TestMain(unittest.TestCase):
    def setUp(self):
        self.deck = Deck()
        self.deck.creation()
        self.main = MainJoueur()

    def test_distribution_main(self):
        self.main.distribution_main(deck=self.deck, taille_main=30)
        self.assertEqual(len(self.main.cartes), 30)  # la main doit contenir les 30 cartes piochées
        for cartes in self.main.cartes[1:]:  # on vérifie que les cartes sont valides (sous forme de code)
            self.assertIn(cartes, [1,2,3,4,5,6,7,8,9,10,11])

    def test_defausse(self):
        self.main.defausser_main()
        self.assertEqual(self.main.cartes, [])
        
if __name__ == '__main__':
    unittest.main()
