import unittest
from model.utilisateur import Utilisateur
from model.solde_joueur import SoldeJoueur

class TestSoldeJoueur(unittest.TestCase):

    def setUp(self):
        self.utilisateur = Utilisateur("Jean", "Dupont", "jdupont", 100)
        self.solde_joueur = SoldeJoueur(self.utilisateur)

    def test_miser_solde_insuffisant(self):
        with self.assertRaises(ValueError):
            self.solde_joueur.miser(200)

    def test_miser_solde_suffisant(self):
        self.solde_joueur.miser(50)
        self.assertEqual(self.solde_joueur.utilisateur.solde, 50)

    def test_gagner(self):
        self.solde_joueur.gagner(25)
        self.assertEqual(self.solde_joueur.utilisateur.solde, 125)

    def test_str(self):
        self.assertEqual(str(self.solde_joueur), "Solde actuel de jdupont : 100 €")

if __name__ == '__main__':
    unittest.main()