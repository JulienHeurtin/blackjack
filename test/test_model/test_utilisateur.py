import unittest
from model.utilisateur import Utilisateur

class TestUtilisateur(unittest.TestCase):
    
    def setUp(self):
        self.utilisateur = Utilisateur("Jean", "Dupont", "JDupont", 1000)

    def test_init(self):
        self.assertEqual(self.utilisateur.prenom, "Jean")
        self.assertEqual(self.utilisateur.nom, "Dupont")
        self.assertEqual(self.utilisateur.pseudo, "JDupont")
        self.assertEqual(self.utilisateur.solde, 1000)

if __name__ == '__main__':
    unittest.main()
