import unittest
import sqlite3
from model.utilisateur import Utilisateur
from dao.gestion_utilisateur import GestionUtilisateur
from dao.db_connection import DBConnection
import dotenv

class TestGestionUtilisateur(unittest.TestCase):

    def setUp(self):
        # Initialisation d'une instance de GestionUtilisateur pour le pseudo "test"
        dotenv.load_dotenv(override=True, dotenv_path=".env.test")
        self.gestionnaire = GestionUtilisateur("test")
        # Ajout d'un utilisateur "test" dans la base de données pour les tests
        self.nouvel_utilisateur = Utilisateur(prenom="John", nom="Doe", pseudo="test", solde=100)

    def test_recuperer_donnees_utilisateur(self):
        # Vérification que la méthode retourne None si l'utilisateur n'existe pas dans la base de données
        self.connection = DBConnection().get_connection()
        self.cursor = self.connection.cursor()
        self.cursor.execute('''DROP TABLE IF EXISTS Utilisateur;''')
        self.cursor.execute('''
        CREATE TABLE Utilisateur(
            pseudo text UNIQUE PRIMARY KEY,
            prenom text,
            nom text,
            solde int);''')
        utilisateur = (self.nouvel_utilisateur.prenom, self.nouvel_utilisateur.nom, self.nouvel_utilisateur.pseudo, self.nouvel_utilisateur.solde)
        requete = "INSERT INTO Utilisateur (prenom, nom, pseudo, solde) VALUES (?, ?, ?, ?)"
        self.cursor.execute(requete, utilisateur)

        gestionnaire_inexistant = GestionUtilisateur("inexistant")
        self.assertIsNone(gestionnaire_inexistant.recuperer_donnees_utilisateur())

        # Vérification que la méthode retourne les informations correctes de l'utilisateur
        self.gestionnaire.recuperer_donnees_utilisateur()
        self.assertEqual(self.gestionnaire.utilisateur.prenom, "John")
        self.assertEqual(self.gestionnaire.utilisateur.nom, "Doe")
        self.assertEqual(self.gestionnaire.utilisateur.solde, 100)

    def test_modifier_solde(self):
        self.connection = DBConnection().get_connection()
        self.cursor = self.connection.cursor()
        self.cursor.execute('''DROP TABLE IF EXISTS Utilisateur;''')
        self.cursor.execute('''
        CREATE TABLE Utilisateur(
            pseudo text UNIQUE PRIMARY KEY,
            prenom text,
            nom text,
            solde int);''')
        utilisateur = (self.nouvel_utilisateur.prenom, self.nouvel_utilisateur.nom, self.nouvel_utilisateur.pseudo, self.nouvel_utilisateur.solde)
        requete = "INSERT INTO Utilisateur (prenom, nom, pseudo, solde) VALUES (?, ?, ?, ?)"
        self.cursor.execute(requete, utilisateur)
        # Vérification que la méthode retourne True si la modification a réussi
        self.gestionnaire.recuperer_donnees_utilisateur()
        self.assertTrue(self.gestionnaire.modifier_solde(200))
        # Vérification que la modification a bien été enregistrée dans la base de données
        self.cursor.execute("SELECT solde FROM Utilisateur WHERE pseudo = ?", (self.nouvel_utilisateur.pseudo,))
        result = self.cursor.fetchone()
        self.assertEqual(result[0], 200)

if __name__ == '__main__':
    unittest.main()
