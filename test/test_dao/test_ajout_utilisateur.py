import unittest
from model.utilisateur import Utilisateur
from dao.ajout_utilisateur import AjoutUtilisateur
import sqlite3
import dotenv 
from dao.db_connection import DBConnection

class TestAjoutUtilisateur(unittest.TestCase):

    def setUp(self):
        dotenv.load_dotenv(override=True, dotenv_path=".env.test")
        self.nouvel_utilisateur = Utilisateur(prenom="Jeremy", nom="LaCascade", pseudo="regis",  solde=100)
        self.ajout_utilisateur = AjoutUtilisateur()
        self.connection = DBConnection().get_connection()
        self.cursor = self.connection.cursor()
        self.cursor.execute('''DROP TABLE IF EXISTS Utilisateur;''')
        self.cursor.execute('''
        CREATE TABLE Utilisateur(
            pseudo text UNIQUE PRIMARY KEY,
            prenom text,
            nom text,
            solde int);''')
    
    def creation_utilisateur(self):
        self.ajout_utilisateur.creer_utilisateur(self.nouvel_utilisateur)

    def test_creer_utilisateur(self):
        # On vérifie que l'utilisateur a bien été ajouté
        connection = sqlite3.connect('joueurs_bj.db')
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM Utilisateur WHERE pseudo=\"regis\"")
        utilisateur_cree = cursor.fetchall()[0]
        connection.commit()
        connection.close()

        self.assertIsNotNone(utilisateur_cree)
        self.assertEqual(utilisateur_cree[1], "Jeremy")
        self.assertEqual(utilisateur_cree[2], "LaCascade")
        self.assertEqual(utilisateur_cree[0], "regis")
        self.assertEqual(utilisateur_cree[3], 100)

    def verification_existence_unique_utilisateur(self):
        resultat = self.ajout_utilisateur.creer_utilisateur(self.nouvel_utilisateur)
        self.assertEqual(resultat, "Le pseudo '{nouvel_utilisateur.pseudo}' existe déjà dans la base de données.")

if __name__ == '__main__':
    unittest.main()
