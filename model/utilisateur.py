class Utilisateur:
    """
    Un utilisateur.
    """

    def __init__(self, prenom: str, nom: str, pseudo: str, solde: int):
        self.prenom = prenom 
        self.nom = nom
        self.pseudo = pseudo
        self.solde = solde 