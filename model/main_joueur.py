from model.deck import Deck

class MainJoueur:
    """ Cette classe permet la gestion de la main d'un joueu.
    Taille de la main représente le nombre de cartes dans une main
    """
    
    def __init__(self):
        self.cartes = []

    def distribution_main(self, deck: Deck, taille_main:int):
        """On vient piocher taille_main de cartes dans le jeu qu'on ajoute à la main.
           Notre main avec les cartes piochés est une listes de cartes sous forme de code:
           2H pour un 2 de coeur (2 of hearts)

        Args:
            deck (Deck): deck duquel on vient piocher les cartes
        """
        if deck.cartes_restantes >= taille_main:
            cartes_piochees= deck.piocher_cartes(taille_main)
            figure = ["JACK","QUEEN","KING"]
            for i in range(len(cartes_piochees)):
                cartes = cartes_piochees[i]
                if cartes["value"] in figure :
                    self.cartes.append(10)
                elif cartes["value"] == "ACE":
                    self.cartes.append(11)
                else :  
                    self.cartes.append(int(cartes["value"]))
        else:
            return "Il n'y a pas assez de cartes dans le deck"

    def defausser_main(self):
        self.cartes = []