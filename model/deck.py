import requests 

class Deck:
    """Cette classe permet l'utilisation de l'api deckofcardsapi 
    """

    def __init__(self):
        self.deck_id = None
        self.cartes_restantes = None

    def creation(self):
        """On récupére un deck de 6 paquets de 52 cartes soit 312 cartes avec un id unique. 
           On ne récupère que l'id du deck ainsi que le nombre de cartes restantes.
           On associe alors les données récupérés à une instance de Deck. 
           On ajoute une condition sur le code de statut de la requête HTTP avec le code 200. Celui certifie que la
           requête est réussie. 
        """
        response = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6")
        if response.status_code == 200:
            deck_data = response.json()
            self.deck_id = deck_data['deck_id']
            self.cartes_restantes = deck_data['remaining']
        else:
            print("Une erreur est apparue en voulant créer le deck")

    def est_melange(self):
        """On vient vérifier que notre deck qu'on retrouve par son id est bien mélangé,
           lors de la création il l'est nécéssairement normalement.
        """
        response = requests.get(f"https://deckofcardsapi.com/api/deck/{self.deck_id}/shuffle/")
        if response.status_code == 200:
            deck = response.json()
            return deck['shuffled']
        else:
            print("Une erreur est apparue en souhaitant accéder au deck")

    def piocher_cartes(self, n:int):
        """Cette fonction permet de piocher n cartes dans un deck que nous avons préalablement créer et que 
        nous récuperrons grâce à son id. 

        Args:
            n (int): Le nombre de cartes que l'on souahite piocher

        Returns:
            pioche : dictionnaire qui contient les cartes que l'on a piochés
        """
        piocher_n_cartes = requests.get(f"https://deckofcardsapi.com/api/deck/{self.deck_id}/draw/?count={n}")
        if piocher_n_cartes.status_code == 200:
            pioche = piocher_n_cartes.json()
            if "error" in pioche:
                return "Il y a plus de cartes à piocher que des cartes restantes dans le deck"
            else:
                self.cartes_restantes = pioche['remaining']
                cartes = pioche["cards"]
                return cartes
        else:
            print("Une erreur est apparu en souhaitant piocher")