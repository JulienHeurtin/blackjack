from model.utilisateur import Utilisateur

class SoldeJoueur:
    """
    Classe qui permet de gérer le solde d'un utilisateur pour le jeu de blackjack.
    """

    def __init__(self, utilisateur: Utilisateur):
        self.utilisateur = utilisateur

    def miser(self, montant: int):
        """
        Permet à l'utilisateur de miser un certain montant.
        """
        if montant > self.utilisateur.solde:
            raise ValueError("Solde insuffisant")
        self.utilisateur.solde -= montant

    def gagner(self, montant: int):
        """
        Ajoute un certain montant aux gains de l'utilisateur.
        """
        self.utilisateur.solde += montant

    def __str__(self):
        return f"Solde actuel de {self.utilisateur.pseudo} : {self.utilisateur.solde} €"
