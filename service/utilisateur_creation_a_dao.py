from model.utilisateur import Utilisateur
from dao.ajout_utilisateur import AjoutUtilisateur

class UtilisateurCreationADao():
    """L'idée est de crée un service qui lie à la dao pour le main.
       Cette classe sert à importer directement l'utilisateur dans la database après sa création
       On instancie cette classe avec l'utilisateur qu'on souhaite ajouter à la database
    """

    def __init__(self, utilisateur: Utilisateur):
        self.utilisateur =utilisateur
    
    def ajout_a_db(self):
        """ c'est la fonction qui ajoute à la database"""
        return AjoutUtilisateur().creer_utilisateur(self.utilisateur)