from model.deck import Deck
from model.main_joueur import MainJoueur
from model.utilisateur import Utilisateur
from service.gestion_pari import GestionPari
from service.solde_final import SoldeFinal

class Blackjack():
    """Classe qui permet la création de toutes les fonctions utiles au blackjack.
       Prend en entrée un deck et deux mains, celle du joueur et du croupier. 
       On garde le même deck tant que l'on rejoue.
       Jeu simplifié, on considère que l'as vaut 11. Il n'est pas possible de doubler ses cartes en cas de pairs.
    """
    
    def __init__(self, utilisateur : Utilisateur):
        self.deck = Deck()
        self.main_joueur= MainJoueur()
        self.main_croupier = MainJoueur()
        self.utilisateur = utilisateur
        self.pari = GestionPari(utilisateur)

    def calcul_cartes(self, liste):
        """Calcul d'une liste, utile pour calculer le score d'un joueur à partir de sa main
        """
        somme=0
        for i in range(len(liste)):
            somme += liste[i]
        return somme
    
    def initialisation_jeu(self):
        """Lance le jeu en créant le deck
        """
        print("Bienvenue à la table de Blackjack")        
        self.deck.creation()
        self.debut_jeu()

    def debut_jeu(self):
        """Lance le jeu en distribuant 2 cartes à chaque joueurs 
        """
        print("Votre solde pour jouer est de {}€".format(self.utilisateur.solde))
        if self.utilisateur.solde == 0:
            print("Nous sommes au regret de vous annoncer qu'il ne vous reste plus d'argent sur votre compte, merci d'avoir jouer")
            exit()
        else:
            self.pari.parier()
            self.main_joueur.distribution_main(self.deck, 2)
            self.main_croupier.distribution_main(self.deck, 2)
            self.print_main()
            self.print_carte_croupier()


    def print_main(self):
        """affiche la main du joueur
        """
        print("Votre main:{}, soit un score de {}".format(self.main_joueur.cartes, self.calcul_cartes(self.main_joueur.cartes)))

    def print_carte_croupier(self):
        """affiche la carte visible du croupier
        """
        print("La carte visible du croupier est:{}".format(self.main_croupier.cartes[0]))

    def augmenter(self):
        """Permet d'ajouter une carte à sa main pour le joueur tant que la valeur de ses cartes 
        est inférieur à 21.
        """
        if self.calcul_cartes(self.main_joueur.cartes) <21:
            self.main_joueur.distribution_main(self.deck, 1)
            self.print_main()
        else:
            self.evaluation()

    def arreter(self):
        """ Le joueur décide de ne plus augmenter et de garder sa main tel quelle 
        """
        self.evaluation()

    def evaluation(self):
        """Vient procéder à l'évaluation de la partie en comparant les mains des deux joueurs
           Le croupier vient piocher tant que son score de carte est inférieur à 16
           Ensuite on compare le score des joueurs et désigne le vainqeur 
        """
        while self.calcul_cartes(self.main_croupier.cartes) < 17:
            self.main_croupier.distribution_main(self.deck, 1)
        joueur_score = self.calcul_cartes(self.main_joueur.cartes)
        croupier_score = self.calcul_cartes(self.main_croupier.cartes)
        if joueur_score > 21:
            print("Vous avez dépassé 21, vous avez perdu ! La main du croupier était{} soit un score de {}".format(self.main_croupier.cartes, self.calcul_cartes(self.main_croupier.cartes)))
        elif croupier_score > 21:
            print("Le croupier a perdu, sa main est {} soit un score de {}. Vous avez gagné !".format(self.main_croupier.cartes, self.calcul_cartes(self.main_croupier.cartes)))
            self.pari.pari_gagne()
            print("Vous avez remporté {}€".format(2*self.pari.mise))
        elif joueur_score > croupier_score:
            print("Vous avez mieux que le croupier, sa main est {} soit {}. Vous avez gagné !".format(self.main_croupier.cartes, self.calcul_cartes(self.main_croupier.cartes)))
            self.pari.pari_gagne()
            print("Vous avez remporté {}€".format(2*self.pari.mise))
        elif croupier_score > joueur_score:
            print("Le croupier gagne, sa main est {}, soit un score de {}".format(self.main_croupier.cartes, self.calcul_cartes(self.main_croupier.cartes)))
        elif croupier_score == joueur_score:
            print("Le croupier gagne, sa main est {} comme la votre, il est gagnat".format(self.main_croupier.cartes))
        SoldeFinal(self.utilisateur).actualisation_solde(self.utilisateur.solde)
        self.rejouer()

    def rejouer(self):
        """Le joueur peut décider ou non de recommencer à jouer 
        """
        choice = input("Voulez vous jouer encore (Oui/Non): ")
        if choice.lower() == 'oui':
            self.main_croupier.defausser_main()
            self.main_joueur.defausser_main()
            self.debut_jeu()
        else:
            print("Merci d'avoir joué")
            exit()
