from model.utilisateur import Utilisateur
from dao.gestion_utilisateur import GestionUtilisateur

class RecupererDonneesUtilisateur():
    """ Cette classe permet de lié au classe de la dao, on peut s'assurer qu'un utilisateur existe dans la base de données.
    On peut aussi récupérer les données à partir de sonn pseudo, utile notamment pour récupérer son solde.
    Cette classe prend donc en entrée la classe GestionUtilisateur issue de la dao.
    """

    def __init__(self, pseudo):
        self.gestion = GestionUtilisateur(pseudo)
    
    def verification_existence_utilisateur_db(self):
        """Cette fonction vérifie que l'utilisateur existe bien dans la base de données"""
        self.gestion.recuperer_donnees_utilisateur()
        if self.gestion.utilisateur.nom == "Null":
            return False
        else:
            return True 

    def recuperation_donnees(self) -> Utilisateur:
        """ Cette fonction permet de récupérer toutes les données de l'utilisateur grâce à son pseudo.
        Elle retourne une instanciation de l'utilisateur avec toutes les données comprises dans la database.
        """
        self.gestion.recuperer_donnees_utilisateur()
        if self.gestion.utilisateur.nom == "Null":
            return None
        else:
            return self.gestion.utilisateur