from model.solde_joueur import SoldeJoueur
from model.utilisateur import Utilisateur

class GestionPari():
    """Cette classe permet la gestion des paris au sein du jeu de blackjack.
    Elle prend en entrée un utilisateur dont elle va gérer le solde durant la partie avec la classe SoldeJoueur.
    Avec cette classe, le joueur peut gérer son solde en pariant ou en gagnant.
    """
        
    def __init__(self, utilisateur: Utilisateur):
        self.solde = SoldeJoueur(utilisateur)
        self.mise = 0
            
    def parier(self):
        """Ici cela permet au joueur de parier une somme, cette dernière soit un nombre entier (sinon gestion de cas limite)
        Cette somme est supprimé de son solde.
        Le joueur est au casino, il ne peut pas jouer sans parier donc une mise de 0 est interdite et les croupiers trouvent 
        même cela provoquant, en effet, tout travail mérite salaire 
        """
        while True: 
            while True:
                choice = input("Combien souhaitez-vous miser ? ")
                try:
                    self.mise = int(choice)
                    if self.mise <= 0:
                        print("Veuillez ne pas provoquer le croupier, veuillez entrer une mise supérieure à zéro")
                    else:
                        break
                except ValueError:
                    print("Veuillez rentrer un nombre entier s'il vous plait")       
            try:
                self.solde.miser(self.mise)
                break
            except ValueError:
                print("Solde insuffisant sur votre compte pour miser cette somme")

    def pari_gagne(self):
        """Le joueur double sa mise en cas de victoire
        """
        self.solde.gagner(2*self.mise)