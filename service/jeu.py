from service.blackjack import Blackjack
from model.utilisateur import Utilisateur
from service.utilisateur_creation_a_dao import UtilisateurCreationADao

utilisateur = Utilisateur(pseudo="Donatien", nom="Daunat", prenom="Tiens", solde=100)
UtilisateurCreationADao(utilisateur).ajout_a_db()
game = Blackjack(utilisateur)
game.initialisation_jeu()
while True:
    choice = input("Voulez vous augmenter ou arreter: ")
    if choice.lower() == 'augmenter':
        game.augmenter()
        if game.calcul_cartes(game.main_joueur.cartes) > 21:
            game.evaluation()
    elif choice.lower() == 'arreter':
        game.arreter()
    else:
        print("Pas possible. Choisir 'augmenter' ou 'arreter', merci.")