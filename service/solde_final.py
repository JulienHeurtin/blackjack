from model.utilisateur import Utilisateur
from dao.gestion_utilisateur import GestionUtilisateur

class SoldeFinal():
    """Cette classe gère le solde de l'utilisateur, et permet notamment après une partie d'actualiser le solde du joueur
    Cette prend en entrée la classe GestionUtilisateur issue de la dao
    """

    def __init__(self, utilisateur : Utilisateur):
        self.gestion = GestionUtilisateur(utilisateur.pseudo)
    
    def actualisation_solde(self, nouveau_solde):
        """ Cette fonction prend en entrée un solde et envoie ce nouveau solde dans la database à l'utilisateur
        qui s'est identifié grâce à son pseudo.
        """
        self.gestion.modifier_solde(nouveau_solde)
