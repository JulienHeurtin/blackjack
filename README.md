# Blackjack



## Explication des règles simplifiées 

Le jeu est un blackjack simplifié. Le but est d'obtenir le score le plus proche de 21 avec ses cartes.
Les cartes sont simplements représentées avec des chiffres, les cartes valent leur chiffre. Les figures valent 10 et l'As vaut 11.
Vous allez joueur contre un croupier.
Vous gagnez lorsque vous faites mieux que le croupier ou que le croupier dépasse 21. SI vous dépassez 21, c'est perdu.

## Comment lancer le code ? 

Il faut venir installer les 4 librairies écrites dans le fichier requirements.txt pour s'assurer que tout va fonctionner correctement.
Ensuite, il suffit simplement d'aller dans le fichier main et de le run avec la dernière version de python. 
Le code va venir crée un nouveau fichier 'joueurs_bj.db' qui est la base de données dans laquelle vous pourrez stocker les utilisateurs que vous crée pour joueur. 
Vous pouvez écrire le code suivant dans le terminal: 
```
pip install inquirer
pip install InquirerPy
pip install python-dotenv
pip install requests
python main.py
```

## Initialisation du jeu

Il va vous être demandé avant de pouvoir jouer de vous connecter ou de créer un compte. Ce compte un fois crée sera stocké dans la base de données. Quand vous créez un compte, il vous est demandé de mettre votre nom, prénom, pseudo et votre solde. 
Le pseudo est unique et vous permet de vous connecter si vous lancez l'application à un autre moment pour jouer.
Pour plus de réalisme, il est conseillé de limiter votre solde. Cependant, lorsque le solde de l'utilisateur tombe à 0 vous ne pouvez plus jouer avec cet utilisateur et il faudra en créer un nouveau. La faillite existe dans un vrai casino, alors pourquoi pas ici ?

## Comment s'assurer qu'il n'y a pas d'erreurs dans le code ? 

Toutes les fonctions et classes ont été testés grâce à des tests unitaires. Pour lance de manière groupée ces tests pour s'assurer que tout marche, il y a 3 commandes suivantes. Vous pouvez les lancer dans le terminal.
```
python -m unittest discover test/test_model -p "test_*.py"
python -m unittest discover test/test_service -p "test_*.py"
python -m unittest discover test/test_dao -p "test_*.py"
```

## Diagramme d'architecture

Ici on peut retouver un diagramme très simple pour expliquer l'achitecture de l'application
Cela se modélise assez simplement, une boite = une interface et des flèches indiquant les protocoles entre les différentes parties du système.

```mermaid
graph TD;
    App[Application de Blackjack] -->|HTTP| OFFAPI(API de jeu de cartes);
```

## Diagramme des classes du package Model 

Ce diagramme décrit les 4 classes utiles dans l'application.

La classe deck est celle qui fait appel à l'API https://deckofcardsapi.com/ pour créer un deck de cartes. Ici, on l'utilise pour créer un deck contenant 6 paquets de cartes mélangés.

La classe MainJoueur permet la gestion de la main du joueur en utilisant la classe Deck.

La classe utilisateur instancie l'utilisateur qui pourra jouer au jeu.

La classe solde permet la gestion du solde d'un utilisateur.

```mermaid
classDiagram
    SoldeJoueur --> Utilisateur : composition
    MainJoueur --|> Deck

    class Deck{
        +int deck_id
        +int cartes_restantes
        +creation()
        +est_melange()
        +piocher_cartes()
    }
    class MainJoueur{
        +list cartes
        +distribution_main()
        +defausser_main()
    }
    class Utilisateur{
        +str prenom
        +str nom
        +str pseudo
        +int solde
    }
    class SoldeJoueur{
        +Utilisateur utilisateur
        +miser()
        +gagner()
    }
```
